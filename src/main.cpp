#include <iostream>
#include <string>
#include <boost/program_options.hpp>
#include <gpsxml.hpp>
#include "processing_fct.hpp"

/*!
 * \todo If multiple files are provided as input, files are merged.
 */

bool verbosity ;
inline void debug( const std::string & str )
{
    if (verbosity ) std::cout << str << std::endl ;
}

inline void debug_nf( const std::string & str )
{
    if (verbosity ) std::cout << str << std::flush ;
}

int main( int argc, char *argv[] )
{

    // Get Command line options
    using namespace std ;
    namespace po = boost::program_options ;
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help" , "Show help message" )
    ("input-file,i" , po::value< vector<string> >()->multitoken() , "File to process" )
    ("output-file,o" , po::value<string>()->required() , "Output file" )
    ("downsample,s","Allow dowsampling of the track")
    ("distance-min,d" , po::value<double>()->default_value( 2500.0 ) , "Minimum distance between track points when downsampling")
    ("verbose,v","Toggle verbose output");

    po::variables_map vm ;
    po::store( po::parse_command_line( argc , argv , desc ) , vm ) ;
    if ( vm.count("help") )
    {
        cout << desc << endl ;
        return false ;
    }
    verbosity = vm.count("verbose") ;
    vector<string> files = vm["input-file"].as< vector<string> >() ;

    // Output file :
    QString outfile( vm["output-file"].as<string>().c_str() );
    gpsxml::GpxFile * processedFile = new gpsxml::GpxFile( outfile ) ;
    processedFile->metadata.name = outfile ;

    // Load data ;
    bool downsample = vm.count("downsample");
    double distance_min = vm["distance-min"].as<double>() ;
    int n = 0 ;
    gpsxml::boundsType bounds ;
    gpsxml::trkType trk ;
    for( vector<string>::iterator filename = files.begin() ; filename != files.end() ; filename++ )
    {
        debug( std::string( "Processing " )  + std::string( filename->c_str() ) ) ;
        trk = process_tracks_to_track( QString(filename->c_str()) , downsample , distance_min ) ;
        
        if ( !trk.trkpts.empty() )
        {
            debug( "No track - trying routes" ) ;
            trk = process_routes_to_track( QString(filename->c_str()) , downsample , distance_min ) ;
        }

        if (  !trk.trkpts.empty() )
        {
            debug( "No points - Going to next file" ) ;
        }
        else
        {

            trk.number = static_cast<unsigned short>(n++) ;
            processedFile->tracks.append( trk );
            processedFile->metadata.bounds += gpsxml::boundaries( trk.trkpts ) ;

            QString bbox = QString("Bounding box : %1\t%2\t%3\t%4").arg(processedFile->metadata.bounds.minLat())
                           .arg(processedFile->metadata.bounds.maxLat())
                           .arg(processedFile->metadata.bounds.minLon())
                           .arg(processedFile->metadata.bounds.maxLon()) ;
            debug(bbox.toStdString()) ;
            debug( " -> done" );
        }
    }
    processedFile->saveToFile();
    return true ;
} ;
