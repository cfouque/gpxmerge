#include "processing_fct.hpp"

#include <iostream>
#include <gpsxml/GpxFile.hpp>
#include <gpsxml/Math.hpp>
#include <gpsxml/Exceptions.hpp>

QList< gpsxml::rteType > sort_routes( QList< gpsxml::rteType > routes )
{

    // Sorting the routes
    int rteIdx = 0 ;
    QList<gpsxml::rteType> sortedRoutes ;
    sortedRoutes.push_back( routes.front() );
    for ( int z = 0 ; z < routes.size() ; z++ )
    {
        using namespace gpsxml ;
        const rteType * route = &( routes.at( rteIdx ) ) ;
        for ( int k = 0 ; k < routes.size() ; k++ )
        {
            double d = harvesine_distance( route->rtepts.back() , routes.at(k).rtepts.front() ) ;
            if ( static_cast<ptType>(route->rtepts.back()) == static_cast<ptType>(routes.at(k).rtepts.front()) || d < 10.0 )
            {
                rteIdx = k ;
                sortedRoutes.push_back( routes.at(k) );
                break ;
            }
        } ;
    }
    return sortedRoutes ;
} ;

gpsxml::trkType process_routes_to_track(QString filename, bool downsample, double distance_min)
{

    // opening file
    gpsxml::GpxFile * currentFile = 0 ;
    try
    {
        currentFile = new gpsxml::GpxFile( filename );
        currentFile->readFromFile();
    }
    catch ( gpsxml::exception &e )
    {
        std::cout << "Error opening " <<  filename.toStdString() << "\n ->" << e.what() << std::endl ;
        exit(1) ;
    }

    // Get sorted routes
    QList<gpsxml::rteType> routes = sort_routes( currentFile->routes ) ;

    // write routes as a track
    QFileInfo fileinfo( filename ) ;
    gpsxml::trkType commontrack ;
    commontrack.name = fileinfo.baseName() ;
//     commontrack.Set("name",true);
    commontrack.cmt = QString("Merged routes from %1").arg(filename) ;
//     commontrack.Set("cmt",true);
    for ( QList<gpsxml::rteType>::const_iterator rte = routes.constBegin() ; rte != routes.constEnd() ; rte++ )
    {
        using namespace gpsxml ;
        if ( downsample )
        {
            int lastidx = 0 ;
            for( int k = 1 ; k < rte->rtepts.size()-1 ; k++ )
            {
                double d = gpsxml::harvesine_distance( rte->rtepts.at(lastidx) , rte->rtepts.at(k) ) ;
                if ( d > distance_min )
                {
                    commontrack.trkpts.append( rte->rtepts.at(k) );
                    lastidx = k ;
                }
            }
        }
        else
        {
            for( int k = 0 ; k < rte->rtepts.size()-1 ; k++ )
            {
                commontrack.trkpts.append( rte->rtepts.at(k) );
            }
        }

    }
//     commontrack.Set("trkpt", !commontrack.trkpts.empty() ) ;
    return commontrack ;
} ;

gpsxml::trkType process_tracks_to_track(QString filename, bool downsample, double distance_min)
{

    // opening file
    gpsxml::GpxFile * currentFile = 0 ;
    try
    {
        currentFile = new gpsxml::GpxFile( filename );
        currentFile->readFromFile();
    }
    catch ( gpsxml::exception &e )
    {
        std::cout << "Error opening " <<  filename.toStdString() << "\n ->" << e.what() << std::endl ;
        exit(1) ;
    }

    // write routes as a track
    QList<gpsxml::trkType> * tracks = &( currentFile->tracks ) ;
    QFileInfo fileinfo( filename ) ;
    gpsxml::trkType commontrack ;
    commontrack.name = fileinfo.baseName() ;
//     commontrack.Set("name",true);
    commontrack.cmt = QString("Merged tracks from %1").arg(filename) ;
//     commontrack.Set("cmt",true);
    for ( QList<gpsxml::trkType>::const_iterator rte = tracks->constBegin() ; rte != tracks->constEnd() ; rte++ )
    {
        using namespace gpsxml ;
        if ( downsample )
        {
            int lastidx = 0 ;
            for( int k = 1 ; k < rte->trkpts.size()-1 ; k++ )
            {
                double d = gpsxml::harvesine_distance( rte->trkpts.at(lastidx) , rte->trkpts.at(k) ) ;
                if ( d > distance_min )
                {
                    commontrack.trkpts.append( rte->trkpts.at(k) );
                    lastidx = k ;
                }
            }
        }
        else
        {
            for( int k = 0 ; k < rte->trkpts.size()-1 ; k++ )
            {
                commontrack.trkpts.append( rte->trkpts.at(k) );
            }
        }

    }
//     commontrack.Set("trkpt", !commontrack.trkpts.empty() ) ;
    return commontrack ;
} ;

