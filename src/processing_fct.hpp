#ifndef __PROCESSING_FONCTION__
#define __PROCESSING_FONCTION__

#include <QtCore>
#include <gpsxml/TypesDefinitions.hpp>

QList<gpsxml::rteType> sort_routes( QList< gpsxml::rteType > routes ) ;

gpsxml::trkType process_routes_to_track( QString filename, bool downsample, double distance_min );

gpsxml::trkType process_tracks_to_track( QString filename, bool downsample, double distance_min );

#endif